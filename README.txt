INTRODUCTION
------------
This module retrieves stock prices from Google and stores them in the database.
Prices are fetched on cron. There is a publicly accessible page at /stocks by
default displaying the stocks and their prices. This page can be changed in the
configuration.

CONFIGURATION
-------------
* Settings at /admin/config/services/stocks
* Stock list and manager at /admin/config/services/stocks/list
