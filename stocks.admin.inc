<?php

/**
 * @file
 * Stocks administration pages.
 */

/**
 * Create the form for the admin settings.
 */
function stocks_admin_settings() {
  $form = array();
  $form['stocks_update'] = array(
    '#prefix' => '<strong>Update frequency</strong>',
    '#markup' => '<p>' . t('Stocks are updated on every cron run.</p>'),
  );
  $form['update'] = array(
    '#type' => 'submit',
    '#value' => 'Update stocks now',
    '#submit' => array('_stocks_update_now'),
  );

  $form['page'] = array(
    '#type' => 'fieldset',
    '#title' => t('Stocks page'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['page']['stocks_page_create'] = array(
    '#type' => 'checkbox',
    '#title' => t('Create a page'),
    '#default_value' => variable_get('stocks_page_create'),
    '#description' => t('Creates a publicly available page showing a list of stocks and their prices.'),
  );
  $form['page']['stocks_page_title'] = array(
    '#type' => 'textfield',
    '#size' => 30,
    '#default_value' => variable_get('stocks_page_title'),
    '#description' => t('The title to give to the stocks page.'),
    '#states' => array(
      'visible' => array(
        ':input[name="stocks_create_page"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['page']['stocks_page_url'] = array(
    '#type' => 'textfield',
    '#size' => 30,
    '#default_value' => variable_get('stocks_page_url'),
    '#description' => t('The url alias to give to the stocks page.'),
    '#states' => array(
      'visible' => array(
        ':input[name="stocks_create_page"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['page']['message'] = array(
    '#markup' => '<p>The cache will need to be cleared after changing these options.</p>',
  );

  $form['stocks_list'] = array(
    '#prefix' => '<p>',
    '#markup' => l(t('View and manage stocks'), 'admin/config/services/stocks/list'),
    '#suffix' => '</p>',
  );

  return system_settings_form($form);
}

/**
 * Stock admin page.
 */
function stocks_list() {
  $output = '<p>' . l(t('Add a stock'), 'admin/config/services/stocks/add') . '</p>';

  $result = db_select('stocks_stocks', 'ss')
    ->fields('ss', array('symbol', 'name'))
    ->extend('PagerDefault')
    ->extend('TableSort')
    ->orderByHeader(array('symbol'))
    ->limit(200)
    ->execute();

  $header = array(
    'symbol' => array(
      'data' => t('Symbol'),
      'field' => 'ss.symbol',
      'class' => array('left'),
    ),
    'name' => array(
      'data' => t('Name'),
      'field' => 'ss.name',
      'class' => array('left'),
    ),
  );

  $rows = array();
  while ($data = $result->fetchObject()) {
    $rows[] = array(
      l($data->symbol, 'admin/config/services/stocks/' . $data->symbol . '/edit'),
      $data->name,
    );
  }

  $output .= theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array(
      'width' => '100%',
      'id' => 'stocktastic-table',
    ),
    'sticky' => TRUE,
    'caption' => NULL,
    'colgroups' => array(),
    'empty' => t('No stocks.'),
  ));
  $output .= theme('pager');

  return $output;
}

/**
 * Implements hook_form().
 *
 * Form to add stocks.
 */
function stocks_add_stock_form($form, &$form_state) {
  $form = array();
  $form['symbol'] = array(
    '#type' => 'textfield',
    '#title' => t('Stock symbol'),
    '#description' => t('Remember to include the exchange prefix for Google (e.g. "NASDAQ:", "NYSE:" etc.)'),
    '#default_value' => '',
    '#required' => TRUE,
  );
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => '',
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add stock'),
  );
  return $form;
}

/**
 * Stock addition form validation.
 */
function stocks_add_stock_form_validate($form, &$form_state) {
  // Check that this stock does not already exist.
  $result = db_select('stocks_stocks', 'ss')
    ->fields('ss', array('symbol'))
    ->condition('ss.symbol', check_plain($form_state['values']['symbol']), '=')
    ->execute()
    ->fetch();

  if ($result) {
    form_set_error('symbol', t('The symbol @symbol already exists.', array(
      '@symbol' => $form_state['values']['symbol'],
    )));
  }
}

/**
 * Stock addition handler.
 */
function stocks_add_stock_form_submit($form, &$form_state) {
  db_insert('stocks_stocks')
    ->fields(array(
      'symbol' => $form_state['values']['symbol'],
      'name' => $form_state['values']['name'],
    ))
    ->execute();

  $symbol = check_plain($form_state['values']['symbol']);
  $edit = l(t('View/Edit it now.'), 'admin/config/services/stocks/' . $symbol . '/edit');
  drupal_set_message(t('Stock %stock saved. !edit', array(
    '%stock' => $form_state['values']['name'],
    '!edit' => $edit,
  )));
  $form_state['redirect'] = 'admin/config/services/stocks/list';
}

/**
 * Implements hook_form().
 *
 * Stock edit form.
 */
function stocks_edit_stock_form($form, &$form_state) {
  $result = db_select('stocks_stocks', 'ss')
    ->fields('ss', array('symbol', 'name', 'price'))
    ->condition('ss.symbol', arg(4), '=')
    ->execute()
    ->fetch();
  $form = array();
  $form['symbol'] = array(
    '#prefix' => '<h1>',
    '#markup' => isset($result->symbol) ? $result->symbol : '',
    '#suffix' => '</h1>',
  );
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => isset($result->name) ? $result->name : '',
    '#required' => TRUE,
  );
  $form['price'] = array(
    '#type' => 'textfield',
    '#title' => t('Last price'),
    '#default_value' => isset($result->price) ? $result->price : '0',
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Edit stock'),
  );
  $delete = user_access('delete stocks');
  if (($delete) === TRUE) {
    $form['delete'] = array(
      '#type' => 'submit',
      '#value' => 'Delete',
      '#submit' => array('_stocks_edit_stock_form_delete'),
    );
  }
  return $form;
}

/**
 * Stock edit handler.
 */
function stocks_edit_stock_form_submit($form, &$form_state) {
  db_update('stocks_stocks')
    ->fields(array(
      'name' => $form_state['values']['name'],
      'price' => $form_state['values']['price'],
    ))
    ->condition('symbol', arg(4), '=')
    ->execute();

  drupal_set_message(t('stock saved'));
}

/**
 * Stock deletion handler.
 */
function _stocks_edit_stock_form_delete($form, &$form_state) {
  $delete = user_access('delete stocks');
  if (($delete) !== TRUE) {
    return FALSE;
  }
  db_delete('stocks_stocks')
    ->condition('symbol', arg(4), '=')
    ->execute();

  drupal_set_message(t('stock deleted'));
  $form_state['redirect'] = 'admin/config/services/stocks/list';
}

/**
 * Action for the button to update all stocks now.
 */
function _stocks_update_now() {
  stocks_fetch_prices();
  drupal_set_message(t('Stocks have been updated'));
}
